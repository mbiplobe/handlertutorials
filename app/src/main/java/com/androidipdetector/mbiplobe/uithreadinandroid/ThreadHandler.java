package com.androidipdetector.mbiplobe.uithreadinandroid;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ThreadHandler extends Activity {

    private TextView threadHandlerTextView;
    private Button showMessageTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_handler);
        threadHandlerTextView= (TextView) findViewById(R.id.threadHandlerTextView);
        showMessageTextView= (Button) findViewById(R.id.showMessageTextView);


        final Handler handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                threadHandlerTextView.setText("I Love you");
            }
        };

        showMessageTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable runnable=new Runnable() {
                    @Override
                    public void run() {

                        handler.sendEmptyMessage(0);
                    }
                };
                Thread thread=new Thread(runnable);
                thread.start();
            }
        });
    }



}
