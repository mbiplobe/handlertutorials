package com.androidipdetector.mbiplobe.uithreadinandroid.progreassbar;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ProgressBar;

import com.androidipdetector.mbiplobe.uithreadinandroid.R;

/**
 * Created by mbiplobe on 10/13/2015.
 */
public class PostProgressInAndroid extends Activity {
    private ProgressBar progressBar;
    private Handler handler;
    private Thread thread;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        for(int i=1;i<=100;i++){
                            progressBar.setProgress(i);

                        }
                    }
                },1000);
            }
        });
        thread.start();
//
    }

//        class MyThread implements Runnable{
//
//            @Override
//            public void run() {
//
//            }
//        }
//
//    }


}
