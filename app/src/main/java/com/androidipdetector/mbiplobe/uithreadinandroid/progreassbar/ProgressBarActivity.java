package com.androidipdetector.mbiplobe.uithreadinandroid.progreassbar;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.androidipdetector.mbiplobe.uithreadinandroid.R;

public class ProgressBarActivity extends Activity {

    private ProgressBar progressBar;
    private Handler handler;
    private Thread thread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);
        progressBar= (ProgressBar) findViewById(R.id.progressBar1);
        thread=new Thread(new myThead());
        thread.start();
        progressBar.setMax(100);
        handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                progressBar.incrementProgressBy(1);
            }
        };

    }

    class myThead implements Runnable {

        @Override
        public void run() {
//            Message message = Message.obtain();
            for (int i = 0; i < 100; i++) {
//                message.arg1 = i;

//                handler.dispatchMessage(message);
//            handler.sendMessageDelayed(message, 100);
            handler.sendMessage(Message.obtain());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


        }
    }
        }



}
